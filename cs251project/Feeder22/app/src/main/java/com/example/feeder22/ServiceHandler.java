package com.example.feeder22;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Iterator;

/**
 * Created by jeyasoorya on 1/11/16.
 */

public class ServiceHandler {
    //static InputStream is = null;
    //static String response = null;
    //public final static int GET = 1;
    //public final static int POST = 2;
    public String authorizationCall(String url, String uname, String pwd)throws Exception{
        JSONObject params=new JSONObject();
        params.put("username",uname);
        params.put("password",pwd);
        return makeServiceCall(params,url);
    }
    public String getFeedbacksCall(String url, String uname)throws Exception{
        JSONObject params=new JSONObject();
        params.put("username",uname);
        return makeServiceCall(params,url);
    }
    public String getDeadlinesCall(String url, String uname)throws Exception{
        JSONObject params=new JSONObject();
        params.put("username",uname);
        return makeServiceCall(params,url);
    }
    public String makeServiceCall(JSONObject params, String url)throws Exception{
        URL site = new URL(url);
        HttpURLConnection urlConnection = (HttpURLConnection) site.openConnection();
        //urlConnection.addRequestProperty("name",uname);
        urlConnection.setRequestMethod("POST");
        urlConnection.setDoInput(true);
        urlConnection.setDoOutput(true);
        OutputStream out = urlConnection.getOutputStream();
        BufferedWriter writer=new BufferedWriter(new OutputStreamWriter(out,"UTF-8"));
        writer.write(getPostDataString(params));
        writer.flush();
        writer.close();
        out.close();
        //urlConnection.addRequestProperty("password",pwd);
        urlConnection.connect();
        int response = urlConnection.getResponseCode();
        if(response==HttpURLConnection.HTTP_OK){
            BufferedReader in=new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            StringBuffer sb=new StringBuffer("");
            String line="";
            while((line=in.readLine())!=null){
                sb.append(line);
                break;
            }
            in.close();
            urlConnection.disconnect();
            return sb.toString();
        }
        else{
            return new String("Connection Error");
        }

    }
    public String getPostDataString(JSONObject params)throws Exception{
        StringBuilder sb = new StringBuilder();
        boolean first = true;
        Iterator<String> itr = params.keys();
        while(itr.hasNext()){
            String key=itr.next();
            Object value=params.get(key);
            if(first)
                first=false;
            else
                sb.append("&");
            sb.append(URLEncoder.encode(key,"UTF-8"));
            sb.append("=");
            sb.append(URLEncoder.encode(value.toString(),"UTF-8"));
        }
        return sb.toString();
    }
}
