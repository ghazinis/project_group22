package com.example.feeder22;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class Main3Activity extends AppCompatActivity {
    ListView listView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);
        SharedPreferences s = (getApplicationContext()).getSharedPreferences("Myprefs", MODE_PRIVATE);
        String itemValue = s.getString("itemValue", null);
        listView = (ListView) findViewById(R.id.ListView02);
        final ArrayList<String> mylist = new ArrayList<String>();
        final ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, mylist);

        for(String w:itemValue.split("\\s",0)){
            mylist.add(w);
        }
        listView.setAdapter(adapter);
    }
}
