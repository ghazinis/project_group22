package com.example.feeder22;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.net.Uri;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;
import android.support.v4.app.FragmentActivity;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import com.google.gson.reflect.TypeToken;
import com.google.gson.Gson;

import com.roomorama.caldroid.CaldroidFragment;
import com.roomorama.caldroid.CaldroidListener;

public class Main2Activity extends FragmentActivity implements CalendarFragment.OnFragmentInteractionListener {
    String uname;
    ListView listView;
    CaldroidFragment caldroidFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        SharedPreferences s = (getApplicationContext()).getSharedPreferences("Myprefs", MODE_PRIVATE);
        uname = s.getString("uname", null);

        caldroidFragment = new CaldroidFragment();
        Bundle args = new Bundle();
        Calendar cal = Calendar.getInstance();
        args.putInt(CaldroidFragment.MONTH, cal.get(Calendar.MONTH) + 1);
        args.putInt(CaldroidFragment.YEAR, cal.get(Calendar.YEAR));
        args.putInt(CaldroidFragment.START_DAY_OF_WEEK, CaldroidFragment.MONDAY);
        caldroidFragment.setArguments(args);
        caldroidFragment.refreshView();
        if (savedInstanceState == null) {
            android.support.v4.app.FragmentTransaction t1 = getSupportFragmentManager().beginTransaction();
            t1.replace(R.id.fragment4, caldroidFragment);
            t1.commit();
        }

        listView = (ListView) findViewById(R.id.ListView01);
        final ArrayList<String> mylist = new ArrayList<String>();
        final ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, mylist);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                String itemValue = (String) listView.getItemAtPosition(position);
                //Toast.makeText(getApplicationContext(),
                  //      "Position :" + position + "  ListItem : " + itemValue, Toast.LENGTH_LONG)
                    //    .show();
                SharedPreferences s =(getApplicationContext()).getSharedPreferences("Myprefs",MODE_PRIVATE);
                SharedPreferences.Editor e = s.edit();
                e.putString("itemValue", itemValue);
                e.commit();
                Intent nextScreen = new Intent(getApplicationContext(), Main3Activity.class);
                startActivity(nextScreen);
            }

        });

        final CaldroidListener listener = new CaldroidListener() {
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            @Override
            public void onSelectDate(Date date, View view) {
                String s1 = formatter.format(date);
                Toast toast = Toast.makeText(getApplicationContext(), s1, Toast.LENGTH_SHORT);
                toast.show();
                //Display the list of events corresponding to that date
                Date startDate;
                mylist.clear();
                if (fd != null) {
                    for (int i = 0; i < fd.length; i++) {
                        try {
                            startDate = formatter.parse(fd[i].fields.deadline);
                            if (startDate.equals(date))
                                mylist.add(fd[i].fields.name + " " + fd[i].fields.course + " " + fd[i].fields.deadline);
                        } catch (java.text.ParseException e) {
                            e.printStackTrace();
                        }
                    }
                }
                if (dd != null) {
                    for (int i = 0; i < dd.length; i++) {
                        try {
                            startDate = formatter.parse(dd[i].fields.deadline);
                            if (startDate.equals(date))
                                mylist.add(dd[i].fields.name + " " + dd[i].fields.course + " " + dd[i].fields.deadline);
                        } catch (java.text.ParseException e) {
                            e.printStackTrace();
                        }
                    }
                }


                // Assign adapter to ListView
                listView.setAdapter(adapter);
            }
        };
        caldroidFragment.setCaldroidListener(listener);
        caldroidFragment.refreshView();

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Date startDate;
        if (fd != null) {
            for (int i = 0; i < fd.length; i++) {
                try {
                    startDate = df.parse(fd[i].fields.deadline);
                    ColorDrawable c = new ColorDrawable(dateMap.get(fd[i].fields.deadline).intValue());
                    caldroidFragment.setBackgroundDrawableForDate(c, startDate);
                    caldroidFragment.refreshView();
                } catch (java.text.ParseException e) {
                    //Log.e("COULD NOT PARSE",e.getMessage());
                    e.printStackTrace();
                }
            }
        }
        if (dd != null) {
            for (int i = 0; i < dd.length; i++) {
                try {
                    startDate = df.parse(dd[i].fields.deadline);
                    ColorDrawable c = new ColorDrawable(dateMap.get(dd[i].fields.deadline).intValue());
                    caldroidFragment.setBackgroundDrawableForDate(c, startDate);
                    caldroidFragment.refreshView();
                } catch (java.text.ParseException e) {
                    //Log.e("COULD NOT PARSE",e.getMessage());
                    e.printStackTrace();
                }
            }
        }
    }

    public void onFragmentInteraction(Uri uri) {
        //Toast toast = Toast.makeText(this, "Wheeee!", Toast.LENGTH_SHORT);
        //toast.show();
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    public void updateDB(View view) {
        new GetFeedbacks().execute(uname);
        new GetDeadlines().execute(uname);
        Intent nextScreen = new Intent(getApplicationContext(), Main2Activity.class);
        //nextScreen.putExtra("name", result);
        nextScreen.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        nextScreen.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        nextScreen.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(nextScreen);
    }


    public void gotologinpage(View view) {
        SharedPreferences s = (getApplicationContext()).getSharedPreferences("Myprefs", MODE_PRIVATE);
        SharedPreferences.Editor e = s.edit();
        e.clear();
        e.commit();
        Intent nextScreen = new Intent(getApplicationContext(), MainActivity.class);
        nextScreen.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        nextScreen.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        nextScreen.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(nextScreen);
    }

    private class GetFeedbacks extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        protected String doInBackground(String... arg) {
            String url = "http://10.0.2.2:8022/feedr/getFeedbacks/";
            String uname = arg[0];
            ServiceHandler s = new ServiceHandler();
            String msg = "";
            try {
                msg = s.getFeedbacksCall(url, uname);
                if (msg.equals("Invalid")) {
                    return "__invalidcre__";
                }
                return msg;
            } catch (Exception e) {
                return "__invalid__";
            }
        }


        protected void onPostExecute(String result) {
            if (result.equals("__invalidcre__")) {
                Toast.makeText(getApplicationContext(), "Invalid Credentials", Toast.LENGTH_LONG).show();
            } else if (result.equals("__invalid__")) {
                Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_LONG).show();
            } else {
                SharedPreferences s = (getApplicationContext()).getSharedPreferences("Myprefs", MODE_PRIVATE);
                SharedPreferences.Editor e = s.edit();
                e.putString("name", result);
                e.commit();
                //Toast.makeText(getApplicationContext(), result, Toast.LENGTH_LONG).show();
                Gson gson = new Gson();
                Type collectionType = new TypeToken<Collection<FeedBacks>>() {
                }.getType();
                Collection<FeedBacks> response = gson.fromJson(result, collectionType);
                fd = response.toArray(new FeedBacks[response.size()]);
                for(int i=0;i<fd.length;i++) {
                    Integer c = courseMap.get(fd[i].fields.course);
                    if(c==null) {
                        Integer next = rng.nextInt(Integer.MAX_VALUE) + 1;
                        courseMap.put(fd[i].fields.course, next);
                    }
                }
                for (int i = 0; i < fd.length; i++) {
                    Integer c = dateMap.get(fd[i].fields.deadline);
                    if(c==null) {
                        Integer next = courseMap.get(fd[i].fields.course);
                        dateMap.put(fd[i].fields.deadline, next);
                    }
                    else {
                        Integer next = courseMap.get(fd[i].fields.course);
                        dateMap.put(fd[i].fields.deadline, next+c);
                    }
                }
                //Toast.makeText(getApplicationContext(), fd[0].fields.name, Toast.LENGTH_LONG).show();
            }


        }

    }
    Random rng = new Random();
    public class Fdb {
        String name;
        String course;
        String deadline;
    }

    public class FeedBacks {
        String model;
        Integer pk;
        Fdb fields;
    }

    static FeedBacks fd[] = null;


    private class GetDeadlines extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        protected String doInBackground(String... arg) {
            String url = "http://10.0.2.2:8022/feedr/getDeadlines/";
            String uname = arg[0];
            ServiceHandler s = new ServiceHandler();
            String msg = "";
            try {
                msg = s.getDeadlinesCall(url, uname);
                if (msg.equals("Invalid")) {
                    return "__invalidcre__";
                }
                return msg;
            } catch (Exception e) {
                return "__invalid__";
            }
        }


        protected void onPostExecute(String result) {
            if (result.equals("__invalidcre__")) {
                Toast.makeText(getApplicationContext(), "Invalid Credentials", Toast.LENGTH_LONG).show();
            } else if (result.equals("__invalid__")) {
                Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_LONG).show();
            } else {
                SharedPreferences s = (getApplicationContext()).getSharedPreferences("Myprefs", MODE_PRIVATE);
                SharedPreferences.Editor e = s.edit();
                e.putString("name", result);
                e.commit();
                //Toast.makeText(getApplicationContext(), result, Toast.LENGTH_LONG).show();
                Gson gson = new Gson();
                Type collectionType = new TypeToken<Collection<Deadlines>>() {
                }.getType();
                Collection<Deadlines> response = gson.fromJson(result, collectionType);
                dd = response.toArray(new Deadlines[response.size()]);
                //Toast.makeText(getApplicationContext(), dd[0].fields.name, Toast.LENGTH_LONG).show();
                for(int i=0;i<dd.length;i++) {
                    Integer next;
                    Integer c = courseMap.get(dd[i].fields.course);
                    if(c==null) {
                        next = rng.nextInt(Integer.MAX_VALUE) + 1;
                        courseMap.put(dd[i].fields.course, next);
                    }
                }
                for (int i = 0; i < dd.length; i++) {
                    Integer c = dateMap.get(dd[i].fields.deadline);
                    if (c == null) {
                        Integer next = courseMap.get(dd[i].fields.course);
                        dateMap.put(dd[i].fields.deadline, next);
                    } else {
                        Integer next = courseMap.get(dd[i].fields.course);
                        dateMap.put(dd[i].fields.deadline, next + c);
                    }
                }
            }

        }

    }

    public class Ddl {
        String name;
        String course;
        String deadline;
    }

    public class Deadlines {
        String model;
        Integer pk;
        Ddl fields;
    }

    static Deadlines dd[] = null;
    static Map<String, Integer> courseMap = new HashMap<String, Integer>();
    static Map<String, Integer> dateMap = new HashMap<String, Integer>();
}

