from django.db import models

class StudentDetails(models.Model):
    name = models.CharField(max_length=100)
    roll_number = models.BigIntegerField(primary_key=True)
    password = models.CharField(max_length=100)
    cpi = models.DecimalField(max_digits=5,decimal_places=2)
    def __str__(self):
        return self.name

class Courses(models.Model):
    name = models.CharField(max_length=100)
    code = models.CharField(max_length=100, primary_key=True)
    students = models.ManyToManyField(StudentDetails)
    def __str__(self):
        return self.code

class Assignments(models.Model):
    name = models.CharField(max_length=100)
    course = models.ForeignKey(Courses)
    deadline = models.DateTimeField(['%Y-%m-%d %H:%M:%S',])
    def __str__(self):
        return self.name

class Feedbacks(models.Model):
    name = models.CharField(max_length=100)
    course = models.ForeignKey(Courses)
    deadline = models.DateTimeField(['%Y-%m-%d %H:%M:%S',])
    def __str__(self):
        return self.name

class Questions(models.Model):
    question = models.CharField(max_length=200)
    ques_type = models.CharField(max_length=100)
    feedback = models.ForeignKey(Feedbacks)
    def __str__(self):
        return self.question

class Answer(models.Model):
    answer = models.CharField(max_length=200)
    ques = models.ForeignKey(Questions)
    def __str__(self):
        return self.answer
