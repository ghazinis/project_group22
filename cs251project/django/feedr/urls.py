from django.conf.urls import url
from . import views

app_name = 'feedr'
urlpatterns = [
    url(r'^login/$', views.login_view, name='login'),
    url(r'^home/*', views.adminHome_view, name='adminHome'), #Check this one out
    url(r'^admin/add/$', views.addCourses_view, name='addCourses'),
    url(r'^admin/manage/$', views.manageCourses_view, name='manageCourses'),
    url(r'^admin/courses/$', views.courses_view, name='courses'),
    url(r'^admin/feedbacks/$', views.feedback_view, name='feedbacks'),
    url(r'^logout/$', views.logout_view, name='logout'),
    url(r'^signup/$', views.sign_up_view, name='signup'),
    url(r'^home/$', views.home_view, name='home'), #Check this one out
    url(r'^enroll/$', views.enroll_view, name='enroll'),
    url(r'^validateStudent/$', views.valid_view, name='valid'),
    url(r'^getFeedbacks/$', views.get_feedbacks_view, name='getFeedbacks'),
    url(r'^getDeadlines/$', views.get_deadlines_view, name='getDeadlines'),
    url(r'^viewDeadlines/$', views.viewDeadlines_view, name='viewDeadlines'),
    url(r'^addDeadlines/$', views.addDeadlines_view, name='addDeadlines'),
    url(r'^responses/$', views.responses_view, name='responses'),
    url(r'^addFeedback/$', views.addFeedback_view, name='addFeedback'),
    url(r'^addQuestion/$', views.addQuestions_view, name='addQuestions'),
    url(r'^Googlelogin/$', views.Googlelogin_view, name='Googlelogin'),
]
