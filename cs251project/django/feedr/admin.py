from django.contrib import admin
from .models import *
# Register your models here.
admin.site.register(StudentDetails)
admin.site.register(Courses)
admin.site.register(Assignments)
admin.site.register(Feedbacks)
admin.site.register(Questions)
admin.site.register(Answer)