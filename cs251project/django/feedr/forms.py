from django import forms
import datetime
from .models import *

class DeadlineForm(forms.Form):
    courseCode = forms.ModelChoiceField(empty_label="Select a Course" ,queryset=Courses.objects.all())
    assignName = forms.CharField()
    deadlineDate = forms.DateTimeField(['%d-%m-%Y %H:%M:%S',])

class SignUpForm(forms.Form):
    username = forms.CharField()
    email = forms.EmailField()
    password = forms.CharField(min_length=4,widget=forms.PasswordInput)
    pwd2 = forms.CharField(min_length=4,widget=forms.PasswordInput)

class CourseDetailForm (forms.Form):
    courseCode = forms.CharField()
    courseName = forms.CharField()
    midSem = forms.DateField(['%d-%m-%Y'])
    endSem = forms.DateField(['%d-%m-%Y'])

class QuestionForm (forms.Form):
    question = forms.CharField()
    ques_type = forms.ChoiceField(widget=forms.RadioSelect, choices=(('Character Field','Textual question'),('Radio Button','Ratings(1 to 5)')))

class EnrollmentForm (forms.Form):
    current_course = forms.CharField()
    students = forms.ModelMultipleChoiceField(widget=forms.CheckboxSelectMultiple, queryset=StudentDetails.objects.all())
