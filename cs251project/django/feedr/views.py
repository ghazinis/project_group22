import datetime
import os
import collections
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, Http404, HttpResponseRedirect, HttpRequest
from django.urls import reverse
from django.core.exceptions import *
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.views.decorators.cache import cache_control
from django.contrib.auth.decorators import user_passes_test
from .forms import *
from.models import *
from apiclient import discovery
from apiclient.discovery import build
import httplib2
import csv
import json
from oauth2client import client
from django.views.decorators.csrf import csrf_exempt
from django.utils.encoding import force_text
from django.core.serializers.json import DjangoJSONEncoder
from django.core.serializers import serialize

# Create your views here.
PATH = os.path.dirname(os.path.realpath(__file__))
#@csrf_protect
@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def login_view(request):
    if not User.objects.filter(username="admin"):
        with open(PATH+'/admin_details.csv', 'r') as csvfile:
            spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
            for row in spamreader:
                u1=User.objects.create_user(username=row[0],password=row[1],email=row[2])
    if request.user is not None:
        if request.user.is_authenticated():
            return HttpResponseRedirect(reverse('feedr:home'))
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['pwd']
        user = authenticate(username=username, password=password)
        if user is not None:
            if username == "admin" and password == "admin":
                login(request, user)
                return HttpResponseRedirect(reverse('feedr:adminHome'))
            login(request, user)
            return HttpResponseRedirect(reverse('feedr:home'))
        else:
            msg = "Invalid Credentials"
            return render(request, 'feedr/login.html', {'error_message': msg,'path':PATH})
    else:
        return render(request, 'feedr/login.html',{'path':PATH})

def logout_view(request):
    logout(request)
    return HttpResponseRedirect(reverse('feedr:login'))

@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required(login_url='/feedr/login/')
def home_view(request):
    return render(request, 'feedr/home.html', {'activeStatus':["active","","","",""],'path':PATH})

def sign_up_view(request):
    if request.method == 'POST':
        form=SignUpForm(request.POST)
        error=""
        if form.is_valid():
            final=form.cleaned_data
            try:
            	User.objects.get(username=final['username'])
            	error="Username already exists"
            	data={'username':final['username'],'email':final['email']}
            	form=SignUpForm(initial=data)
            	return render(request,'feedr/signup.html',{'path':PATH,'form':form,'uerror':error})
            except:
            	if final['pwd2']!=final['password']:
            		error="Passwords do not match"
            		return render(request,'feedr/signup.html',{'path':PATH,'form':form,'uerror':error})
            	user = User.objects.create_user(username=final['username'], email=final['email'], password=final['password'])
            	login(request, user)
            return HttpResponseRedirect(reverse('feedr:home'))
        else:
        	#print form.errors
    	    return render(request, 'feedr/signup.html',{'path':PATH,'form':form})    	
    form = SignUpForm(initial={'username':"",'email':""})
    return render(request, 'feedr/signup.html',{'path':PATH,'form':form})

@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required(login_url='/feedr/login/')
def addDeadlines_view(request):
    if request.method=='POST':
        form=DeadlineForm(request.POST)
        if form.is_valid():
            final=form.cleaned_data
            sub_date=Assignments(name=final['assignName'],course=final['courseCode'],deadline=final['deadlineDate'])
            sub_date.save()
            return HttpResponseRedirect(reverse('feedr:home'))
        return render(request,'feedr/addDeadlines.html/',{'activeStatus':["","","active","",""],'path':PATH,'form':form})    
    form=DeadlineForm()
    return render(request,'feedr/addDeadlines.html/',{'activeStatus':["","","active","",""],'path':PATH,'form':form})

@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required(login_url='/feedr/login/')
def viewDeadlines_view(request):
    courseList = Courses.objects.all()
    deadlineList=[]
    name=["Running Deadlines","Running Feedbacks","Finished Deadlines","Finished Feedbaks"]
    if request.method=='POST' and request.POST['course']!="All":
        crs=Courses.objects.get(code=request.POST['course'])
        deadlineList.append(Assignments.objects.filter(course=crs,deadline__gte=datetime.date.today()).order_by("-deadline"))
        deadlineList.append(Feedbacks.objects.filter(course=crs,deadline__gte=datetime.date.today()).order_by("-deadline"))
        deadlineList.append(Assignments.objects.filter(course=crs,deadline__lte=datetime.date.today()).order_by("-deadline"))
        deadlineList.append(Feedbacks.objects.filter(course=crs,deadline__lte=datetime.date.today()).order_by("-deadline"))
        return render(request,'feedr/viewDeadlines.html',{'course':crs.code,'activeStatus':["","","","","active"],'deadlineList':deadlineList,'courseList':courseList,'name':name,'path':PATH})        
    deadlineList.append(Assignments.objects.filter(deadline__gte=datetime.date.today()).order_by("-deadline"))
    deadlineList.append(Assignments.objects.exclude(deadline__gte=datetime.date.today()).order_by("-deadline"))
    return render(request,'feedr/viewDeadlines.html',{'activeStatus':["","","","","active"],'deadlineList':deadlineList,'courseList':courseList,'name':name,'path':PATH})

@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required(login_url='/feedr/login/')
def addFeedback_view(request):    
    form=DeadlineForm()
    return render(request,'feedr/addFeedback.html/',{'activeStatus':["","active","","",""],'path':PATH,'form':form})    

@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required(login_url='/feedr/login/')
def addQuestions_view(request):
    if request.method=='POST' and 'Feedback' in request.POST:
        form=DeadlineForm(request.POST)
        if form.is_valid():
            final=form.cleaned_data
            fb1=Feedbacks(name=final['assignName'],course=final['courseCode'],deadline=final['deadlineDate'])
            fb1.save()
            form = QuestionForm()
            return render(request,'feedr/addQuestions.html/',{'activeStatus':["","active","","",""],'path':PATH,'form':form,'current_feedback':fb1})
        return render(request,'feedr/addFeedback.html/',{'activeStatus':["","active","","",""],'path':PATH,'form':form})    
    elif request.method=='POST':
        form=QuestionForm(request.POST)
        if form.is_valid():
            final = form.cleaned_data
            fb1 = Feedbacks.objects.filter(name=request.POST['feedback'],course=Courses.objects.get(code=request.POST['course']))[0]
            ques = Questions(question=final['question'],ques_type=final['ques_type'],feedback=fb1)
            ques.save()
            if 'button1' in request.POST:
                questions = Questions.objects.filter(feedback=fb1)
                form = QuestionForm()
                return render(request,'feedr/addQuestions.html/',{'activeStatus':["","active","","",""],'path':PATH,'form':form,'current_feedback':fb1,'questions':questions}) 
            elif 'button2' in request.POST:
                return HttpResponseRedirect(reverse('feedr:home'))
    return HttpResponseRedirect(reverse('feedr:home'))

@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required(login_url='/feedr/login/')
def responses_view(request):
    crs = Courses.objects.all()
    if request.method == 'POST' and 'Feedback' in request.POST:
        feedbacks = Feedbacks.objects.filter(course=Courses.objects.get(code=request.POST['course']))
        questions = Questions.objects.filter(feedback=Feedbacks.objects.filter(course=Courses.objects.get(code=request.POST['course']),name=request.POST['Feedback'])[0])
        responses = {}
        for q in questions:
            res = Answer.objects.filter(ques=q)
            if q.ques_type == "Character Field":
                responses[q]=res
            else:
                resp = {'1':0,'2':0,'3':0,'4':0,'5':0}
                for r in res:
                    resp[r.answer] = resp[r.answer]+1
                res = collections.OrderedDict(sorted(resp.items()))
                responses[q]=res
        return render(request,'feedr/viewResponses.html',{'path':PATH,'activeStatus':["","","","active",""],'courselist':crs,'feedbacklist':feedbacks,'questionlist':questions,'responselist':responses,'course':request.POST['course'],'Feedback':request.POST['Feedback']})
    elif request.method == 'POST':
        feedbacks = Feedbacks.objects.filter(course=Courses.objects.get(code=request.POST['course']))
        return render(request,'feedr/viewResponses.html',{'path':PATH,'activeStatus':["","","","active",""],'courselist':crs,'feedbacklist':feedbacks,'course':request.POST['course']})
    else:
        return render(request,'feedr/viewResponses.html',{'path':PATH,'activeStatus':["","","","active",""],'courselist':crs})

#Admin part (more or less)
def admincheck(user):
    return (user.username == 'admin')

@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required(login_url='/feedr/login/')
def adminHome_view(request):
    courseList = Courses.objects.all()
    enroll = False
    if not StudentDetails.objects.all():
        enroll = True
    return render(request, 'feedr/home.html', {'activeStatus':["active","",""],'path':PATH,'courseList':courseList, 'enroll':enroll}) 

@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required(login_url='/feedr/login/')
@csrf_exempt
def courses_view(request):
    courseList = Courses.objects.all()
    if request.method == 'POST':
        crs_code = request.POST['course']
        course = Courses.objects.get(code=crs_code)
        return render(request, 'feedr/course.html', {'activeStatus':["active","",""],'path':PATH,'courseList':courseList, 'current_course':course}) 
    else:
        return render(request, 'feedr/home.html', {'activeStatus':["active","",""],'path':PATH,'courseList':courseList}) 

@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required(login_url='/feedr/login/')
def feedback_view(request):
    courseList = Courses.objects.all()
    if request.method == 'POST':
        fdb_name = request.POST['feedback']
        fdb = Feedbacks.objects.filter(name=fdb_name)[0]
        return render(request, 'feedr/feedback.html', {'activeStatus':["active","",""],'path':PATH,'courseList':courseList, 'current_feedback':fdb}) 
    else:
        return render(request, 'feedr/home.html', {'activeStatus':["active","",""],'path':PATH,'courseList':courseList})
    
@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required(login_url='/feedr/login/')
@user_passes_test(admincheck,login_url='/feedr/home')
def addCourses_view(request):
    if request.method == 'POST':
        form = CourseDetailForm(request.POST)
        if form.is_valid():
            final = form.cleaned_data
            if Courses.objects.filter(code=final['courseCode']):
                msg = "Another Course with same course code already exists!"
                return render(request, 'feedr/addCourses.html', {'activeStatus':["","active",""],'form':form,'path':PATH,'error_message':msg})
            else:
                c1 = Courses(name=final['courseName'],code=final['courseCode'])
                c1.save()
                mid_sem = Assignments(name="MidSemester Exam", course=c1, deadline=final['midSem'])
                end_sem = Assignments(name="EndSemester Exam", course=c1, deadline=final['endSem'])
                mid_sem.save()
                end_sem.save()
                fb1 = Feedbacks(name="Midsem Feedback", course=c1, deadline=final['midSem'])
                fb2 = Feedbacks(name="Endsem Feedback", course=c1, deadline=final['endSem'])
                fb1.save()
                fb2.save()
                for f in [fb1,fb2]:
                    with open(PATH +'/questions.csv', 'r') as csvfile:
                        spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
                        for row in spamreader:
                            q1=Questions(question=row[0],ques_type=row[1],feedback=f)
                            q1.save()
                return HttpResponseRedirect(reverse('feedr:home'))
        return render(request, 'feedr/addCourses.html', {'activeStatus':["","active",""],'form':form,'path':PATH}) 
    form = CourseDetailForm()
    return render(request, 'feedr/addCourses.html', {'activeStatus':["","active",""],'form':form,'path':PATH}) 

@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required(login_url='/feedr/login/')
@user_passes_test(admincheck,login_url='/feedr/home')
def manageCourses_view(request):
    courseList = Courses.objects.all()
    if request.method == 'POST':
        if 'course' in request.POST:
            c1 = Courses.objects.get(code=request.POST['course']) 
            form = EnrollmentForm()
            form.fields["students"].queryset = StudentDetails.objects.all().exclude(courses=c1)
            return render(request, 'feedr/manageCourses.html', {'activeStatus':["","","active"], 'courseList':courseList, 'form':form, 'path':PATH, 'current_course':c1})
        else:
            form = EnrollmentForm(request.POST)
            if form.is_valid():
                final = form.cleaned_data
                c1 = Courses.objects.get(code=final['current_course']) 
                for st in final['students']:
                    c1.students.add(st)
                c1.save()
                return HttpResponseRedirect(reverse('feedr:home'))
            else:
                return HttpResponseRedirect(reverse('feedr:home'))
    return render(request, 'feedr/manageCourses.html', {'activeStatus':["","","active"],'courseList':courseList, 'path':PATH}) 

@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required(login_url='/feedr/login/')
@user_passes_test(admincheck,login_url='/feedr/home')
def enroll_view(request):
    with open(PATH+'/student_details.csv', 'r') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
        for row in spamreader:
            s1=StudentDetails(name=row[0],roll_number=int(row[1]),password=row[2],cpi=float(row[3]))
            s1.save()
    courseList = Courses.objects.all()
    return HttpResponseRedirect(reverse("feedr:home"))

# Social Login
@csrf_exempt
def Googlelogin_view(request):
    #if request.is_ajax():
     #   print "hello"
    #print request.body
    auth_code=request.body
    CLIENT_SECRET_FILE = 'client_secrets.json'
    credentials = client.credentials_from_clientsecrets_and_code(
        CLIENT_SECRET_FILE,
        ['https://www.googleapis.com/auth/drive.appdata', 'profile', 'email', 'https://www.googleapis.com/auth/userinfo.profile'],
        auth_code)
    #print credentials.id_token.items()
    user_info_service = build(
      serviceName='oauth2', version='v2',
      http=credentials.authorize(httplib2.Http()))
    user_info = user_info_service.userinfo().get().execute()
    try:
        user=User.objects.get(username=user_info['name'])
    except:
        user = User(username=user_info['name'],email=user_info['email'],password=user_info['id'])
        user.save()
    login(request,user)
    return HttpResponse("Hello")


#Android Related Parts
@csrf_exempt
def valid_view(request):
    if request.method=='POST':
        try:
            list_of_studs = StudentDetails.objects.filter(roll_number=int(request.POST['username']))
        except:
            return HttpResponse("Invalid")
        if list_of_studs:
            if list_of_studs[0].password==request.POST['password']:
                return HttpResponse(list_of_studs[0].name)
            else:
                print ("wrong pwd")
    return HttpResponse("Invalid")

@csrf_exempt
def get_feedbacks_view(request):
    if request.method=='POST':
        try:
            list_of_studs = StudentDetails.objects.filter(name=request.POST['username'])
        except:
            return HttpResponse("No such student")
        if list_of_studs:
            st = list_of_studs[0]
            fdb = Feedbacks.objects.filter(course__in=st.courses_set.all())
            data = serialize('json', fdb)
            return HttpResponse(data)
    return HttpResponse("Invalid")

@csrf_exempt
def get_deadlines_view(request):
    if request.method=='POST':
        try:
            list_of_studs = StudentDetails.objects.filter(name=request.POST['username'])
        except:
            return HttpResponse("No such student")
        if list_of_studs:
            st = list_of_studs[0]
            asgn = Assignments.objects.filter(course__in=st.courses_set.all())
            data = serialize('json', asgn)
            return HttpResponse(data)
    return HttpResponse("Invalid")
