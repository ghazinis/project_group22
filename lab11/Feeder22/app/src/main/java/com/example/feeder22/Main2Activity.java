package com.example.feeder22;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class Main2Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        //Intent i=getIntent();
        //String name=i.getStringExtra("name");
        SharedPreferences s=(getApplicationContext()).getSharedPreferences("Myprefs",MODE_PRIVATE);
        String name=s.getString("name",null);
        TextView t=(TextView)findViewById(R.id.welcome);
        t.setText(name);
    }
    @Override
    public void onBackPressed() {
        finish();
    }
    public void gotologinpage(View view){
        SharedPreferences s=(getApplicationContext()).getSharedPreferences("Myprefs",MODE_PRIVATE);
        SharedPreferences.Editor e=s.edit();
        e.clear();
        e.commit();
        Intent nextScreen = new Intent(getApplicationContext(), MainActivity.class);
        nextScreen.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        nextScreen.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        nextScreen.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(nextScreen);
    }
}
