import datetime
import os
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.urls import reverse
from django.core.exceptions import *
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.views.decorators.cache import cache_control
from .forms import *
from.models import *
from apiclient import discovery
import httplib2
import csv
from oauth2client import client
from django.views.decorators.csrf import csrf_exempt
# Create your views here.
PATH = os.path.dirname(os.path.realpath(__file__))
#@csrf_protect


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
def login_view(request):
    if not User.objects.filter(username="admin"):
        with open(PATH+'/admin_details.csv', 'r') as csvfile:
            spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
            for row in spamreader:
                u1=User.objects.create_user(username=row[0],password=row[1],email=row[2])
    if request.user is not None:
        if request.user.is_authenticated():
            return HttpResponseRedirect(reverse('feedr:home'))
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['pwd']
        user = authenticate(username=username, password=password)
        if user is not None:
            if username == "admin" and password == "admin":
                login(request, user)
                return HttpResponseRedirect(reverse('feedr:adminHome'))
            login(request, user)
            return HttpResponseRedirect(reverse('feedr:home'))
        else:
            msg = "Invalid Credentials"
            return render(request, 'feedr/login.html', {'error_message': msg,'path':PATH})
    else:
        return render(request, 'feedr/login.html',{'path':PATH})


def logout_view(request):
    logout(request)
    return HttpResponseRedirect(reverse('feedr:login'))

@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required(login_url='/feedr/login/')


def home_view(request):
    return render(request, 'feedr/home.html', {'activeStatus':["active","","","",""],'path':PATH})


def sign_up_view(request):
    if request.method == 'POST':
        form=SignUpForm(request.POST)
        if form.is_valid():
            final=form.cleaned_data
            user = User.objects.create_user(username=final['username'], email=final['email'], password=final['password'])
            login(request, user)
            return HttpResponseRedirect(reverse('feedr:home'))
    form = SignUpForm()
    print(form.as_table())
    return render(request, 'feedr/signup.html',{'path':PATH,'form':form})


def addDeadlines_view(request):
    if request.method=='POST':
        form=DeadlineForm(request.POST)
        print form.errors
        if form.is_valid():
            print "hello"
            final=form.cleaned_data
            #sub_date=Assignments(name=,course=,deadline=)
            #sub_date.save()
            print final['courseCode']
            return HttpResponseRedirect('http://localhost/feedr/home')
    form=DeadlineForm()
    return render(request,'feedr/addDeadlines.html/',{'activeStatus':["","","active","",""],'form':form,'path':PATH})


def viewDeadlines_view(request):
    deadlineList=[]
    name=["Running Deadilnes","Finished Deadlines"]
    deadlineList.append(Assignments.objects.filter(deadlineDate__gte=datetime.date.today()).order_by("-deadlineDate"))
    deadlineList.append(Assignments.objects.exclude(deadlineDate__gte=datetime.date.today()).order_by("-deadlineDate"))
    return render(request,'feedr/viewDeadlines.html',{'activeStatus':["","","","","active"],'deadlineList':deadlineList,'name':name,'path':PATH})


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required(login_url='/feedr/login/')
def adminHome_view(request):
    courseList = Courses.objects.all()
    enroll = False
    if not StudentDetails.objects.all():
        enroll = True
    return render(request, 'feedr/home.html', {'activeStatus':["active","",""],'path':PATH,'courseList':courseList, 'enroll':enroll}) 


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required(login_url='/feedr/login/')
def courses_view(request):
    courseList = Courses.objects.all()
    if request.method == 'POST':
        crs_name = request.POST['course']
        course = Courses.objects.get(name=crs_name)
        return render(request, 'feedr/course.html', {'activeStatus':["active","",""],'path':PATH,'courseList':courseList, 'current_course':course}) 
    else:
        return render(request, 'feedr/home.html', {'activeStatus':["active","",""],'path':PATH,'courseList':courseList}) 


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required(login_url='/feedr/login/')
def feedback_view(request):
    courseList = Courses.objects.all()
    if request.method == 'POST':
        fdb_name = request.POST['feedback']
        fdb = Feedbacks.objects.filter(name=fdb_name)[0]
        return render(request, 'feedr/feedback.html', {'activeStatus':["active","",""],'path':PATH,'courseList':courseList, 'current_feedback':fdb}) 
    else:
        return render(request, 'feedr/home.html', {'activeStatus':["active","",""],'path':PATH,'courseList':courseList})
    

@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required(login_url='/feedr/login/')
def addCourses_view(request):
    if request.method == 'POST':
        form = CourseDetailForm(request.POST)
        if form.is_valid():
            final = form.cleaned_data
            if Courses.objects.filter(code=final['courseCode']):
                msg = "Another Course with same course code already exists!"
                return render(request, 'feedr/addCourses.html', {'activeStatus':["","active",""],'form':form,'path':PATH,'error_message':msg})
            else:
                c1 = Courses(name=final['courseName'],code=final['courseCode'])
                c1.save()
                mid_sem = Assignments(name="MidSemester Exam", course=c1, deadline=final['midSem'])
                end_sem = Assignments(name="EndSemester Exam", course=c1, deadline=final['endSem'])
                mid_sem.save()
                end_sem.save()
                fb1 = Feedbacks(name="Midsem Feedback", course=c1, deadline=final['midSem'])
                fb2 = Feedbacks(name="Endsem Feedback", course=c1, deadline=final['endSem'])
                fb1.save()
                fb2.save()
                for f in [fb1,fb2]:
                    with open(PATH +'/questions.csv', 'r') as csvfile:
                        spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
                        for row in spamreader:
                            q1=Questions(question=row[0],ques_type=row[1],feedback=f)
                            q1.save()
                return HttpResponseRedirect(reverse('feedr:home'))
        return render(request, 'feedr/addCourses.html', {'activeStatus':["","active",""],'form':form,'path':PATH}) 
    form = CourseDetailForm()
    return render(request, 'feedr/addCourses.html', {'activeStatus':["","active",""],'form':form,'path':PATH}) 


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required(login_url='/feedr/login/')
def manageCourses_view(request):
    courseList = Courses.objects.all()
    if request.method == 'POST':
        if 'course' in request.POST:
            c1 = Courses.objects.get(name=request.POST['course']) 
            form = EnrollmentForm()
            form.fields["students"].queryset = StudentDetails.objects.all().exclude(courses=c1)
            return render(request, 'feedr/manageCourses.html', {'activeStatus':["","","active"], 'courseList':courseList, 'form':form, 'path':PATH, 'current_course':c1})
        else:
            form = EnrollmentForm(request.POST)
            if form.is_valid():
                final = form.cleaned_data
                c1 = Courses.objects.get(name=final['current_course']) 
                for st in final['students']:
                    c1.students.add(st)
                c1.save()
                return HttpResponseRedirect(reverse('feedr:home'))
            else:
                return HttpResponseRedirect(reverse('feedr:home'))
    return render(request, 'feedr/manageCourses.html', {'activeStatus':["","","active"],'courseList':courseList, 'path':PATH}) 


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required(login_url='/feedr/login/')
def enroll_view(request):
    with open(PATH+'/student_details.csv', 'r') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
        for row in spamreader:
            s1=StudentDetails(name=row[0],roll_number=int(row[1]),password=row[2],cpi=float(row[3]))
            s1.save()
    courseList = Courses.objects.all()
    return render(request, 'feedr/home.html', {'activeStatus':["active","",""],'path':PATH,'courseList':courseList, 'enroll':False})


#Android Related Parts
@csrf_exempt
def valid_view(request):
    if request.method=='POST':
        try:
            list_of_studs = StudentDetails.objects.filter(roll_number=int(request.POST['username']))
        except:
            return HttpResponse("Invalid")
        if list_of_studs:
            if list_of_studs[0].password==request.POST['password']:
                return HttpResponse("Welcome "+list_of_studs[0].name)
            else:
                print ("wrong pwd")
    return HttpResponse("Invalid")