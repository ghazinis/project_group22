from django import forms
import datetime
from .models import *

def courseList():
	crs=Courses.objects.all()
	courseList=[]
	i=1
	for course in crs:
		t=(i,(course.code))
		courseList.append(t)
		i=i+1
	return courseList

class SignUpForm(forms.Form):
	username = forms.CharField()
	email = forms.EmailField()
	password = forms.CharField(min_length=4,widget=forms.PasswordInput)
	pwd2 = forms.CharField(min_length=4,widget=forms.PasswordInput)

class DeadlineForm(forms.Form):
    courseCode = forms.ChoiceField(choices=courseList())
    assignName = forms.CharField()
    deadlineDate = forms.DateTimeField(['%d-%m-%Y %H:%M:%S',])

class CourseDetailForm (forms.Form):
    courseCode = forms.CharField()
    courseName = forms.CharField()
    midSem = forms.DateField(['%d-%m-%Y'])
    endSem = forms.DateField(['%d-%m-%Y'])

class EnrollmentForm (forms.Form):
    current_course = forms.CharField()
    students = forms.ModelMultipleChoiceField(widget=forms.CheckboxSelectMultiple, queryset=StudentDetails.objects.all())
