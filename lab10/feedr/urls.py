from django.conf.urls import url
from . import views

app_name = 'feedr'
urlpatterns = [
    url(r'^login/$', views.login_view, name='login'),
    url(r'^home/$', views.adminHome_view, name='adminHome'),
    url(r'^admin/add/$', views.addCourses_view, name='addCourses'),
    url(r'^admin/manage/$', views.manageCourses_view, name='manageCourses'),
    url(r'^admin/courses/$', views.courses_view, name='courses'),
    url(r'^admin/feedbacks/$', views.feedback_view, name='feedbacks'),
    url(r'^logout/$', views.logout_view, name='logout'),
    url(r'^signup/$', views.sign_up_view, name='signup'),
    url(r'^home/$', views.home_view, name='home'),
    url(r'^enroll/$', views.enroll_view, name='enroll'),
    url(r'^addDeadlines/$', views.addDeadlines_view, name='addDeadlines'),
    url(r'^viewDeadlines/$', views.viewDeadlines_view, name='viewDeadlines'),
    url(r'^validateStudent/$', views.valid_view, name='valid'),
]
